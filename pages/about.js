export default function Fetch(props) {
  console.log(props);

  const add = async () => {
    await fetch('https://api.countapi.xyz/update/bybaboba?amount=1');
  }

  const grab = async () => {
    await fetch('https://api.countapi.xyz/update/bybaboba?amount=-1');
  }

  return(
    <>
      <h1>item page</h1>
      <button onClick={add}>+1</button>
      <button onClick={grab}>-1</button>
      <p>`Amount: ${props.data.value}`</p>
    </>
  );
}


export async function getServerSideProps() {
  const res = await fetch('https://api.countapi.xyz/get/bybaboba');
  const data = await res.json();
  console.log(data);

  return {
    props: {
      data
    }
  }
}